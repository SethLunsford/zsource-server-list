﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;

namespace zSource_Server_List
{
    public partial class Main : Form
    {
        
        public Main()
        {
            InitializeComponent();
        }
        private void zSL_Load(object sender, EventArgs e)
        {
            using (var webClient = new WebClient())
            {
                var jsonString = webClient.DownloadString("http://zsource.org/content/servers.json");
                var data = JsonConvert.DeserializeObject<ZSourceData>(jsonString);
                foreach (var server in data.Servers)
                {
                    var lvi = new ListViewItem(new[] { server.Name, server.Description, server.Game, server.Hostname });
                    serverList.Items.Add(lvi);
                }
            }
        }

        private void serverList_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            foreach (int i in serverList.SelectedIndices)
            {
                servernameLabel.Text = serverList.Items[i].Text;
                descriptionLabel.Text = serverList.Items[i].SubItems[1].Text;
                gameLabel.Text = serverList.Items[i].SubItems[2].Text;
                hostLabel.Text = serverList.Items[i].SubItems[3].Text;
            }
        }

        private void connect_Click(object sender, EventArgs e)
        {
            Process.Start(@"steam://connect/" + hostLabel.Text);
        }
    }
    internal class Server
    {
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("description")]
        public string Description { get; set; }
        [JsonProperty("game")]
        public string Game { get; set; }
        [JsonProperty("hostname")]
        public string Hostname { get; set; }
    }

    internal class ZSourceData
    {
        [JsonProperty("servers")]
        public List<Server> Servers { get; private set; }
    }
}
