﻿namespace zSource_Server_List
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.connect = new System.Windows.Forms.Button();
            this.titleLabel = new System.Windows.Forms.Label();
            this.name1 = new System.Windows.Forms.Label();
            this.desc1 = new System.Windows.Forms.Label();
            this.game1 = new System.Windows.Forms.Label();
            this.host1 = new System.Windows.Forms.Label();
            this.serverList = new System.Windows.Forms.ListView();
            this.servernameLabel = new System.Windows.Forms.Label();
            this.descriptionLabel = new System.Windows.Forms.Label();
            this.gameLabel = new System.Windows.Forms.Label();
            this.hostLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // connect
            // 
            this.connect.Location = new System.Drawing.Point(242, 152);
            this.connect.Name = "connect";
            this.connect.Size = new System.Drawing.Size(351, 54);
            this.connect.TabIndex = 1;
            this.connect.Text = "Connect!";
            this.connect.UseVisualStyleBackColor = true;
            this.connect.Click += new System.EventHandler(this.connect_Click);
            // 
            // titleLabel
            // 
            this.titleLabel.AutoSize = true;
            this.titleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.titleLabel.Location = new System.Drawing.Point(360, 11);
            this.titleLabel.Name = "titleLabel";
            this.titleLabel.Size = new System.Drawing.Size(123, 16);
            this.titleLabel.TabIndex = 2;
            this.titleLabel.Text = "zSource Server List";
            // 
            // name1
            // 
            this.name1.AutoSize = true;
            this.name1.Location = new System.Drawing.Point(242, 49);
            this.name1.Name = "name1";
            this.name1.Size = new System.Drawing.Size(70, 13);
            this.name1.TabIndex = 3;
            this.name1.Text = "Server name:";
            // 
            // desc1
            // 
            this.desc1.AutoSize = true;
            this.desc1.Location = new System.Drawing.Point(249, 71);
            this.desc1.Name = "desc1";
            this.desc1.Size = new System.Drawing.Size(63, 13);
            this.desc1.TabIndex = 4;
            this.desc1.Text = "Description:";
            // 
            // game1
            // 
            this.game1.AutoSize = true;
            this.game1.Location = new System.Drawing.Point(274, 95);
            this.game1.Name = "game1";
            this.game1.Size = new System.Drawing.Size(38, 13);
            this.game1.TabIndex = 5;
            this.game1.Text = "Game:";
            // 
            // host1
            // 
            this.host1.AutoSize = true;
            this.host1.Location = new System.Drawing.Point(239, 119);
            this.host1.Name = "host1";
            this.host1.Size = new System.Drawing.Size(73, 13);
            this.host1.TabIndex = 6;
            this.host1.Text = "Hostname/IP:";
            // 
            // serverList
            // 
            this.serverList.Location = new System.Drawing.Point(12, 11);
            this.serverList.Name = "serverList";
            this.serverList.Size = new System.Drawing.Size(221, 195);
            this.serverList.TabIndex = 8;
            this.serverList.UseCompatibleStateImageBehavior = false;
            this.serverList.View = System.Windows.Forms.View.List;
            this.serverList.SelectedIndexChanged += new System.EventHandler(this.serverList_SelectedIndexChanged_1);
            // 
            // servernameLabel
            // 
            this.servernameLabel.AutoSize = true;
            this.servernameLabel.Location = new System.Drawing.Point(318, 49);
            this.servernameLabel.Name = "servernameLabel";
            this.servernameLabel.Size = new System.Drawing.Size(0, 13);
            this.servernameLabel.TabIndex = 9;
            // 
            // descriptionLabel
            // 
            this.descriptionLabel.AutoSize = true;
            this.descriptionLabel.Location = new System.Drawing.Point(317, 71);
            this.descriptionLabel.Name = "descriptionLabel";
            this.descriptionLabel.Size = new System.Drawing.Size(0, 13);
            this.descriptionLabel.TabIndex = 10;
            // 
            // gameLabel
            // 
            this.gameLabel.AutoSize = true;
            this.gameLabel.Location = new System.Drawing.Point(316, 95);
            this.gameLabel.Name = "gameLabel";
            this.gameLabel.Size = new System.Drawing.Size(0, 13);
            this.gameLabel.TabIndex = 11;
            // 
            // hostLabel
            // 
            this.hostLabel.AutoSize = true;
            this.hostLabel.Location = new System.Drawing.Point(317, 119);
            this.hostLabel.Name = "hostLabel";
            this.hostLabel.Size = new System.Drawing.Size(0, 13);
            this.hostLabel.TabIndex = 12;
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(601, 216);
            this.Controls.Add(this.hostLabel);
            this.Controls.Add(this.gameLabel);
            this.Controls.Add(this.descriptionLabel);
            this.Controls.Add(this.servernameLabel);
            this.Controls.Add(this.serverList);
            this.Controls.Add(this.host1);
            this.Controls.Add(this.game1);
            this.Controls.Add(this.desc1);
            this.Controls.Add(this.name1);
            this.Controls.Add(this.titleLabel);
            this.Controls.Add(this.connect);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(617, 255);
            this.Name = "Main";
            this.Text = "Server List";
            this.Load += new System.EventHandler(this.zSL_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button connect;
        private System.Windows.Forms.Label titleLabel;
        private System.Windows.Forms.Label name1;
        private System.Windows.Forms.Label desc1;
        private System.Windows.Forms.Label game1;
        private System.Windows.Forms.Label host1;
        private System.Windows.Forms.ListView serverList;
        private System.Windows.Forms.Label servernameLabel;
        private System.Windows.Forms.Label descriptionLabel;
        private System.Windows.Forms.Label gameLabel;
        private System.Windows.Forms.Label hostLabel;
    }
}

